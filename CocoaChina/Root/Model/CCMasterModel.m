//
//  CCMasterModel.m
//  CocoaChina
//
//  Created by Zhang on 15/11/7.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCMasterModel.h"

@implementation CCMasterModel


//===========================================================
//  Keyed Archiving
//
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.newsTitle forKey:@"newsTitle"];
    [encoder encodeObject:self.newsImage forKey:@"newsImage"];
    [encoder encodeObject:self.newsLink forKey:@"newsLink"];
    [encoder encodeObject:self.newsType forKey:@"newsType"];
    [encoder encodeObject:self.newsTypeName forKey:@"newsTypeName"];
    [encoder encodeInteger:self.newsNum forKey:@"newsNum"];
    [encoder encodeObject:self.newsSource forKey:@"newsSource"];
    [encoder encodeObject:self.newsCreateTime forKey:@"newsCreateTime"];
    [encoder encodeObject:self.newsId forKey:@"newsId"];
    [encoder encodeBool:self.read forKey:@"read"];
    [encoder encodeObject:self.objectId forKey:@"objectId"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.newsTitle = [decoder decodeObjectForKey:@"newsTitle"];
        self.newsImage = [decoder decodeObjectForKey:@"newsImage"];
        self.newsLink = [decoder decodeObjectForKey:@"newsLink"];
        self.newsType = [decoder decodeObjectForKey:@"newsType"];
        self.newsTypeName = [decoder decodeObjectForKey:@"newsTypeName"];
        self.newsNum = [decoder decodeIntegerForKey:@"newsNum"];
        self.newsSource = [decoder decodeObjectForKey:@"newsSource"];
        self.newsCreateTime = [decoder decodeObjectForKey:@"newsCreateTime"];
        self.newsId = [decoder decodeObjectForKey:@"newsId"];
        self.read = [decoder decodeBoolForKey:@"read"];
        self.objectId = [decoder decodeObjectForKey:@"objectId"];
    }
    return self;
}

- (BOOL)isEqual:(CCMasterModel *)other{
    if (other == self) {
        return YES;
    } else if (![super isEqual:other]) {
        return NO;
    } else {
        return [other.newsId isEqualToString:self.newsId];
    }
}

- (NSUInteger)hash{
    
    return [self.newsId hash];
}
@end
