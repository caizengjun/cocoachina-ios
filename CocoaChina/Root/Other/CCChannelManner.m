//
//  YJChannelManner.m
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCChannelManner.h"

@interface CCChannelManner ()
@property (nonatomic, strong) NSArray *channelArray;
@end

@implementation CCChannelManner

+ (instancetype)sharedManager{
    static CCChannelManner *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[CCChannelManner alloc]init];
    });
    return _sharedManager;
}

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

@end
