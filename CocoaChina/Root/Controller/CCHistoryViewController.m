//
//  YJReadHistoryViewController.m
//  yuedu
//
//  Created by Zhang on 15/10/21.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCHistoryViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "YJHistroyService.h"
#import "MJRefreshAutoStateFooter.h"
#import "CCHistoryModel.h"
#import "CCNavigationBar.h"
#import "CCMasterTableViewCell.h"
#import "CCWebViewController.h"

@interface CCHistoryViewController ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, assign) NSInteger offset;
@property (nonatomic, assign) BOOL refreshing;
@property (nonatomic, strong) UIButton *backButton;
@end

@implementation CCHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self commonInit];
    
    self.limit = 50;
    self.offset = 0;
    self.dataSource = [[NSMutableArray alloc]init];
    
    __weak typeof(self) weakSelf = self;
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        weakSelf.refreshing = NO;
        weakSelf.offset += weakSelf.limit;
        [self loadMoreData];
    }];
    footer.triggerAutomaticallyRefreshPercent = -20;
    footer.stateLabel.font = [UIFont systemFontOfSize:13];
    footer.stateLabel.textColor = [UIColor yj_lightTextColor];
    [footer setTitle:@"没有更多了" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    //[self loadAds];
    [self loadMoreData];
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor yj_cellBackgroundColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navBar];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(64, 0, 0, 0));
    }];
}

#pragma mark - data
-(void)loadMoreData{
    NSArray *historyList = [[YJHistroyService sharedYJHistroyService]historyListByOffset:self.offset limit:self.limit];
    if (historyList.count < self.limit) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
    [self.dataSource addObjectsFromArray:historyList];
    [self.tableView reloadData];
    [self.tableView reloadEmptyDataSet];
}

#pragma mark - UIScrollViewDelelgate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

}
#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CCMasterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMasterCellIdentifier forIndexPath:indexPath];
    CCHistoryModel *historyModel = self.dataSource[indexPath.row];
    cell.model = historyModel.masterCellModel;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CCHistoryModel *historyModel = self.dataSource[indexPath.row];
    CCMasterModel *masterCellModel = historyModel.masterCellModel;
    CCMasterTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.model = masterCellModel;
    
    CCWebViewController *webViewController = [[CCWebViewController alloc]init];
    webViewController.masterModel = masterCellModel;
    [self showViewController:webViewController sender:self];
}

#pragma mark - DZNEmptyDataSetDelegate
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return self.dataSource.count == 0;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [[NSAttributedString alloc]initWithString:@"您还没有阅读过任何文章" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
}

-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{

    return [UIImage imageNamed:@"not_found"];
}

-(CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{

    return -80;
}

#pragma mark - DZNEmptyDataSetSource

#pragma mark - action
-(void)onBackButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - getters and setters

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.title = @"阅读历史";
        _navBar.leftButton = self.backButton;
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[CCMasterTableViewCell class] forCellReuseIdentifier:kMasterCellIdentifier];
        _tableView.backgroundColor = [UIColor yj_cellBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 90;
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
