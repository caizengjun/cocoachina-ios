//
//  YJURL.h
//  yuedu
//
//  Created by Zhang on 15/9/17.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#ifndef YJURL_h
#define YJURL_h

/**
 微信精选文章数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param rand    int     参数值1为随机获取，返回数量跟随num参数
 @param word    string  检索关键词，返回数量跟随num参数
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_WXNEW_OTHER = @"http://api.huceo.com/wxnew/other/";

/**
 微信营销类文章数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_WXEM_OTHER = @"http://api.huceo.com/wxem/other/";

/**
 社会新闻数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_SOCIAL_OTHER = @"http://api.huceo.com/social/other/";

/**
 国内新闻数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_GUONEI_OTHER = @"http://api.huceo.com/guonei/other/";

/**
 国际新闻数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_WORLD_OTHER = @"http://api.huceo.com/world/other/";

/**
 体育新闻数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_TIYU_OTHER = @"http://api.huceo.com/tiyu/other/";

/**
 娱乐花边数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_HUABIAN_OTHER = @"http://api.huceo.com/huabian/other/";

/**
 美女图片数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_MEINV_OTHER = @"http://api.huceo.com/meinv/other/";

/**
 科技新闻数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_KEJI_OTHER = @"http://api.huceo.com/keji/other/";

/**
 奇闻异事数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_QIWEN_OTHER = @"http://api.huceo.com/qiwen/other/";

/**
 健康资讯数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_HEALTH_OTHER = @"http://api.huceo.com/health/other/";

/**
 旅游热点数据
 @param key     string  API密钥（请在个人中心获取）
 @param num     int     指定返回数量，最大50
 @param page    int     翻页，每页输出数量跟随num参数
 */
static NSString *const kURL_TRAVEL_OTHER = @"http://api.huceo.com/travel/other/";


#pragma mark - URL
/**
 文章——查询微信文章详情接口
 */
//static NSString *const kURL_SHOWAPI_582_2 =@"http://apis.baidu.com/showapi_open_bus/weixin/weixin_article_list";
static NSString *const kURL_SHOWAPI_582_2 =@"http://route.showapi.com/582-2";
/**
 坐标查询城市
 */
static NSString *const kURL_SHOWAPI_9_5 =@"http://route.showapi.com/9-5";

/**
 ip查询城市
 */
static NSString *const kURL_SHOWAPI_9_4 =@"http://route.showapi.com/9-4";



static NSString *const kURL_YUEJI_LOGIN = @"http://api.ppox.com.cn/yueji/login.php";


#endif /* DAMURL_h */
