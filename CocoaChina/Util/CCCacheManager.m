//
//  YJCacheManager.m
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCCacheManager.h"

NSString *const KEY_CACHE_DIRECTORY = @"AdCache";

@interface CCCacheManager ()
///缓存路径
@property (nonatomic, copy) NSString *cachePath;

@end

@implementation CCCacheManager

+ (instancetype)sharedManager{
    
    static CCCacheManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[CCCacheManager alloc]init];
    });
    return _sharedManager;
}

///缓存路径
- (NSString *)cachePath{
    if (_cachePath == nil) {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        _cachePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:KEY_CACHE_DIRECTORY];
        if (![fileMgr fileExistsAtPath:_cachePath]) {
            [fileMgr createDirectoryAtPath:_cachePath withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    return _cachePath;
}

///根据KEY值保存缓存
- (BOOL)saveCacheData:(id)data forFey:(NSString *)key{
    if (!data) {
        return NO;
    }
    __block BOOL success;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *fileName = [self.cachePath stringByAppendingPathComponent:key];
        success = [NSKeyedArchiver archiveRootObject:data toFile:fileName];
    });
    return success;
}

///根据URL和参数保存缓存数据
- (BOOL)saveCacheData:(id)data withUrlPath:(NSString *)urlString andParameters:(NSDictionary *)parameters{
    
    NSString *key = [self cacheKeyForURL:urlString parameter:parameters];
    return [self saveCacheData:data forFey:key];
}

///根据访问路径和参数返回缓存的key值
- (NSString *)cacheKeyForURL:(NSString *)urlString parameter:(NSDictionary *)parameter{
    
    if (parameter == nil) return [YJTools md5:urlString];
    NSMutableDictionary *mutableParameters = [parameter mutableCopy];
    [mutableParameters removeObjectForKey:@"showapi_timestamp"];
    [mutableParameters removeObjectForKey:@"showapi_sign"];
    NSString *stringWitForm  = [NSString stringWithFormat:@"%@%@",urlString,mutableParameters.description];
    return [YJTools md5:stringWitForm];
}

///根据KEY值返回缓存数据
- (id)cacheforKey:(NSString *)key{
    
    return  [NSKeyedUnarchiver unarchiveObjectWithFile:[self.cachePath stringByAppendingPathComponent:key]];
}

///根据URL和参数返回缓存数据
- (void)cacheForPath:(NSString *)path parameters:(NSDictionary *)parameters completion:(DAMCacheBlock)comleteBlock{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *cacheKey = [self cacheKeyForURL:path parameter:parameters];
        id cacheData = [self cacheforKey:cacheKey];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (comleteBlock && cacheData) {
                //如果没有缓存数据则不调用回调block
                comleteBlock(cacheData);
            }
        });
    });
}

@end
